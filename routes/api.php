<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CheeseController; 

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('api')->get('/cheese', [CheeseController::class, 'index'])->name("cheese");
Route::middleware('api')->get('cheese/add', [CheeseController::class, 'addCheese']);
Route::middleware('api')->post('/cheese', [CheeseController::class, 'store'])->name("add.cheese");

