<?php

namespace App\Http\Controllers;

use App\Models\Marker;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\DB;

class MarkerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Marker::all();
        return Inertia::render('Map', ['data' => $data]);
    }



    public function store(Request $request)
    {
        Marker::create([
            'name' => $request->name,
            'longitude' => $request->lng,
            'latitude' => $request->lat,
            'description' => $request->description,
        ]);
        return redirect()->back();
    }

    public function show($id)
    {
        $marker = Marker::find($id);
        return Inertia::render('EditPage', ['marker' => $marker]);
    }

    public function update(Request $request, $id)
    {
        $marker = Marker::findOrFail($id);

        $marker->update($request->all());
        
        return redirect('/map');
    }

    public function destroy($id)
    {
        $marker = Marker::find($id);
        $marker->delete();
        return redirect()->back();
    }
}
