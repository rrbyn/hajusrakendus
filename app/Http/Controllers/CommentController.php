<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Blog;
use App\Models\Comment;
use Inertia\Inertia;

class CommentController extends Controller
{
    public function store(Blog $blog, Request $request){
        $data = $request->all();
        $blog->comments()->create([
            'comment' => $data["comment"]
        ]);

        return redirect()->back();
    }

    public function delete($id){
        $comment = Comment::find($id);
        $comment->delete();

        return redirect()->back();
    }
}
