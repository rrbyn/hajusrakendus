<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Inertia\Inertia;

class WeatherController extends Controller
{
    public function index()
    { 
        $apiKey = config("services.weather.key");
        $lastModified = date('H:i:s', Storage::lastModified('example.txt')); 
        $currentTime = date('H:i:s', time());
        $timeToUpdate = date('H:i:s', strtotime($lastModified. " +5 minutes"));
        

        if ($currentTime > $timeToUpdate) {
            $response = Http::get("https://api.openweathermap.org/data/2.5/weather?lat=58.254951&lon=22.491898&appid={$apiKey}&units=metric");

            Storage::disk('local')->put('example.txt', $response);
        }

        $cachedResponse = json_decode(Storage::get('example.txt'), true);

        return Inertia::render('Weather', ['data' => $cachedResponse, 'lastUpdated' => $lastModified]);
    }
}
