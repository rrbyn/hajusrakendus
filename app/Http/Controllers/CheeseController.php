<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cheese;
use Inertia\Inertia;

class CheeseController extends Controller
{
    public function index(){
        $cheese = Cheese::all();

        return $cheese;
    }

    public function addCheese(){
        return Inertia::render("CheeseForm");
    }

    public function store(Request $request){
        $data = $request->all();
        if($request->file('image')){
            $file = $request->file('image');
            $filename = date('YmdHi').$file->getClientOriginalName();
            $file->move(public_path('Image'), $filename);
            $data['image'] = 'images/'.$filename;
        }
        return $data;
        $cheese = Cheese::create($data);

        return $cheese;
    }
}
