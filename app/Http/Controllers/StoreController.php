<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Inertia\Inertia;
use Validator;

class StoreController extends Controller
{
    public function index(){
        $res = Http::get("https://epood.ta19heinsoo.itmajakas.ee/api/hajus");
        $data = json_decode($res->body(), true);
        return Inertia::render('Store', ['data' => $data]);
    }

    public function cartList(){
        $data = session()->get('cart');
        $sendData = array();
        if($data){
            $stripe = new \Stripe\StripeClient(config("services.stripe.sk"));
            foreach ($data as $value) {
                $product = $stripe->products->create([
                    'name' => $value["title"],
                    'description' => $value["description"],
                    'images' => ["https://epood.ta19heinsoo.itmajakas.ee/".$value["image"]],
                    'default_price_data' => [
                        'currency' => 'eur',
                        'unit_amount' => (int) $value["price"] * 100
                    ],
                    'metadata' => ['amount' => $value['qty']]
                ]);

                array_push($sendData, $product);
            }
        }

        return Inertia::render('Cart', [
            'data' => $data,
            'cart' => $sendData,
            'appURL' => config("services.app.url")
        ]);
    }

    public function cartCount(){
        $data = session()->get('cart');
    }
    

    public function cartAdd(Request $request )
    {
        $product = $request->all();
        $validator = Validator::make($request->all(), [
            'qty' => 'required|numeric|gt:01' 
            ])->passes(); 
            $qty = $validator ? $request->qty : 1; 
            $product['quantity'] = $qty;
            if (session()->has('cart.'.$product['id'])){
                $cart_product = session('cart.'.$product['id'],$product); 
                $cart_product['quantity'] += $qty;
                session()->put('cart.'.$product['id'],$cart_product ); 
            }else{ 
                session()->put('cart.'.$product['id'],$product ); 
        } 
        return redirect()->back(); 
    } 
    public function cartUpdate(Request $request) {
            $product = $request->all();
            $validator = Validator::make($request->all(), [
                'qty' => 'required|numeric|gt:0'
            ])->passes();
    
            if(session()->has('cart.'.$product['id']) && $validator){
                $cart_product = session('cart.'.$product['id'], $product);
                $cart_product['qty'] = $request['qty'];
                session()->put('cart.'.$product['id'],$cart_product);
            }
            return redirect()->back();
        } 
         
    function success(){
        session()->flush();
        return redirect('/store');
    }
        
    public function destroy($id){ 
        session()->forget('cart.'.$id);
        return redirect()->back(); 
    }
} 
